# Dat Mini Comic Scroll Style

This is a template for making a mini comic dat zine, where it's displayed as one long scroll.  Right now, it has an existing mini-comic as it's sample display, called 'Clear Skin in an Unclear World'

# Customizing

So you copied a zine and wanna make it your own.  This is sweet!  This particular dat zine is done 'minicomic style', and to customize it takes just three steps:

**1.) Set this to yr 'local directory' and replace the comic-pages with your own.
2.) add your deets to distro, to better share yr zine with others.
3.) customize the aesthetic to match your own.**

## Important Pre-Step: Know your Folders!

Before going into the customization, let's do a brief rundown of the file structure of this zine.

Your files look something like this:

```
aesthetic/
comic-pages/
distro/
javascripts/
index.html
strange-named-files.json
```
`aesthetic/` holds all the files for styling: your CSS stylesheets and your font files. You'll be editing this a bit.
`comic-pages/` holds the image files that'll make up the bulk of this zine. You'll be replacing this entirely with yr own stuff.
`distro/` is for adding this zine to dat-zine-libraries, and for putting in your authoratative authorial details.  You'll be replacing this entirely too.
`javascripts`  This holds the different bits of code logic to make all this work.  It's recommended to not edit this unless you know what you are doing, or you wanna live dangerously. But you shouldn't have to edit it anyway(assuming I knew what I was doing.)
`index.html` This is the main page, the actual site people visit when they get yr datlink.  You shouldn't have to edit this.  If you do, try to only add things and take nothing away (as that could mess up the site).
`strange-named-files` these just keep the dat archive stuff running.  You shouldn't need to edit these directly.

Now you know your files.  Let's make a minicomic!

## 1.) Replace the comic Pages with your own.

First, in the bottom right click 'set local directory' and choose where you'd wanna store the files that make up this mini comic.  This can make them easier to edit and make sure you always have a copy.  

THEN!

The `comic-pages` folder holds all the images.  They'll look something like this:

```
cover.jpg
1.jpg
2.jpg
3.jpg
4.jpg
back.jpg
```
You can simply open up this folder on your computer, delete all the files, and drag your own in.  (You can also just drag them direct into the beaker browser).

**Important note!**
The files can be gif, png, or jpg but they should match these naming conventions exactly.  This  ensures each page shows up in the right order on the main site.  So please rename your files from 'myComic-page1-final_trueFinal.png` to just '1.png'

## 2.) Edit your distro deets

Zines are best when shared with others.  The `distro` folder helps with that.  It's made up of two parts:
- a cover
- an info.txt file

The cover can just be a copy of the cover file you put in `comic-pages/`.  It can be a gif, png, jpg, or jpeg but n eeds to be called 'cover'.

the info.txt page is where you can put in your name, the comic title, and any other notes.  These are used to fill out the info box on the main site, and used to provide info to the various dat zine libraries this comic will end up in.

It is best to follow the format that you see already in the info.txt page.  It's important that there are exactly 4 dashes seperating each part, for example, to make sure your info is parsed correctly by zine libraries.  

## 3.) Edit yr Aesthetic

You don't have to do this if you like the style of the minicomic as it is now.  but if you want to make it more of your own, go to the `aesthetic` folder.  The majority of this is CSS files.  

If you don't wanna dive deep into CSS, then the easiest thing is to just go to `colors-and-fonts.css`. You can browser to it in beaker, and then edit the file in beaker too.

Here you'll find all the main elements of the site, and you can change the color used for each one.  Just replace the color that's there with your hexcode or web colorname of choice and press save. 

if you wanna dive into the css, check out `main.css`.  I tried to make the classes explicit enough so it was easy enough to edit.  If yr having difficult, lemme know at (webmaster@coolguy.website


# That's it, make it your own and send me your zine when it's done if you'd like!

you can send it to me at webmaster@coolguy.website or on scuttlebutt where my pub key is `@ZqH7Mctu/7DNInxuwl12ECjfrAKUX2tBLq1rOldNhg0=.ed25519`

(oh, and when you make the zine, rewrite this README to make it your own and get your own zines sent to ya.)
