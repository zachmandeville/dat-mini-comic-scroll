export const $ = (el, sel = false) => {
  if (typeof sel === 'string') {
    // (el, sel)
    return el.querySelector(sel)
  }
  // (sel)
  return document.querySelector(el)
}

export const clone = (templateEl) => {
  return document.importNode(templateEl.content, true)
}

export const sansExtension = (fileName) => {
  return fileName.split('.')[0]
}

export const asc = (a, b) => {
  var first = Number(sansExtension(a))
  var second = Number(sansExtension(b))
  return first - second
}
