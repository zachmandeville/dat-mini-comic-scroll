import {$, clone, sansExtension, asc} from './utilities.js'

const archive = new DatArchive(window.location)

function onPageLoad () {
  //read the /comic-pages folder, then add the files,sorted by page number, to the site.
  archive.readdir('/comic-pages').then(dir => addComic(dir.sort(asc)))
}

function addComic (dir) {
  addCover(dir)
  addPages(dir)
  addBack(dir)
}

function addCover (comics) {
  // section defines where, within index.html, the cover should be placed
  var section = {
    template: '#cover-template',
    className: '.cover',
    id: '#cover'
  }
  var coverImage = findInDir(comics, 'cover')
  assignToSection(section, coverImage)
}

function addPages (comics) {
  // section defines where, within index.html, the pages should be placed.
  var section = {
    template: '#pages-template',
    className: '.page',
    id: '#pages'
  }
  var pages = findPages(comics)
  assignToSection(section, pages)
}

function findInDir (arr, fileName) {
  return arr.find(file => sansExtension(file) === fileName)
}

function findPages (arr) {
  return arr.filter(file => {
    file = sansExtension(file)
    return file !== 'cover' && file !== 'back'
  })
}

function addBack (comics) {
  // section defines where, within index.html, the cover should be placed
  var section = {
    template: '#back-template',
    className: '.back',
    id: '#back'
  }
  var backImage = findInDir(comics, 'back')
  assignToSection(section, backImage)
}

function assignToSection (section, image) {
  if (section.id === '#cover' || section.id === '#back') {
    // find the respective template, clone it and add the src Image to it, then aappend it to its section within index.html
    var element = clone($(section.template))
    $(element, section.className).src = `comic-pages/${image}`
    $(section.id).appendChild(element)
  } else {
    for (var page of image) {
    // iterate over the pages, appending each one to <div id='pages'>, using the same method as the cover and back pages.
      var comicsPage = clone($('#pages-template'))
      $(comicsPage, '.page').src = `comic-pages/${page}`
      $('#pages').appendChild(comicsPage)
    }
  }
}

function readInfo () {
  archive.readFile('distro/info.txt')
    .then(file => {
      var betterFile = file.split('----')
      grabTitle(betterFile)
      grabAuthors(betterFile)
      renderInfo(file)
    })
}

function grabTitle (text) {
  var fullTitle = text.find(string => string.match('title'))
  var title = fullTitle.replace('title:', '').replace('\n', '')
  $('#title').textContent = title
  $('title').textContent = title
}

function grabAuthors (text) {
  var fullAuthors = text.find(string => string.match('authors'))
  var authors = fullAuthors
    .replace('author', '')
    .replace('s:', '')
    .replace('\n', '')
  $('#authors').textContent = authors
  console.log({authors})
}

function renderInfo (text) {
  $('#info').textContent = text
}

async function createCopy () {
  $('#info').classList.toggle('invisible')
  $('#copy-box').classList.toggle('invisible')
  var newZine = await DatArchive.fork(archive, { title: 'name your zine here!', description: 'Please Read the README once the site forks (found in your files).  but also, write the description of your zine now!'})
  window.open(newZine.url)
}

// Run that page load function Baby!! Set this whole thing in MOTION
onPageLoad()
readInfo()
$('#copy-button').onclick = createCopy
