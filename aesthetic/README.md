# A E S T H E T I C

This folder hodls all the styling we use in the mini-comic, and you have free reign to change all of it.

You don't have to do this if you like the style of the minicomic as it is now.  But you ain't bound to my gentle geocities fashion.


If you don't wanna dive deep into CSS, then the easiest place to start is to just go to `colors-and-fonts.css`. You can browse to it in beaker, and then edit the file in beaker too.

Here you'll find all the main elements of the site, and you can change the color used for each one.  Just replace the color that's there with your hexcode or web colorname of choice and press save. 

if you wanna dive into the css, check out `main.css`.  I tried to make the classes explicit enough so it was easy enough to edit.  If yr having difficult, lemme know at (webmaster@coolguy.website

