# The Distro Folder

Zines are best when shared with others.  The `distro` folder helps with that.  It's made up of two parts:
- a cover
- an info.txt file

The cover can just be a copy of the cover file you put in `comic-pages/`.  It can be a gif, png, jpg, or jpeg but n eeds to be called 'cover'.

the info.txt page is where you can put in your name, the comic title, and any other notes.  These are used to fill out the info box on the main site, and used to provide info to the various dat zine libraries this comic will end up in.

It is best to follow the format that you see already in the info.txt page.  It's important that there are exactly 4 dashes seperating each part, for example, to make sure your info is parsed correctly by zine libraries.  

